package com.itau;

import java.util.Random;

public class Exercicio1 {
    public static void main(String[] args) {

        Random random = new Random();

        int numero = random.nextInt(6) + 1;
        System.out.println("Exercicio 1: sortear um número aleatório");
        System.out.println("Número sorteado no dado: " + numero);

        System.out.println("\nExercicio 2: sortear três números e sua soma");
        int soma = 0;

        for(int x = 0; x < 3; x++){
            int num = random.nextInt(6) + 1;
            System.out.print(num + " , ");
            soma += num;
        }
        System.out.print("SOMA: " + soma + "\n");

        System.out.println("\nExercicio 3: sortear três grupos de três numeros e sua soma");
        for(int i = 0; i < 3; i++){
            soma = 0;
            for(int j = 0; j < 3; j++){
                int num1 = random.nextInt(6) + 1;
                System.out.print(num1 + " , ");
                soma += num1;
            }
            System.out.println("SOMA: " + soma );
        }
    }
}